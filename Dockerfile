# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:latest as build

# Set environment variables (with default values)
ARG STORCK_CLIENT_ID
ARG CLIENT_ID
ARG API_URL

RUN export STORCK_CLIENT_ID=$STORCK_CLIENT_ID
RUN export CLIENT_ID=$CLIENT_ID
RUN export API_URL=$API_URL

# Set the working directory
WORKDIR /usr/local/app

# Add the source code to app
COPY ./ /usr/local/app/

# Install all the dependencies
RUN npm install

# Generate 'environment.prod.ts' config file
RUN node set-env.js

# Generate the build of the application
RUN npm run build


# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:latest

# Copy the build output to replace the default nginx contents.
COPY --from=build /usr/local/app/dist/storck-vis /usr/share/nginx/html

# Expose port 80
EXPOSE 80