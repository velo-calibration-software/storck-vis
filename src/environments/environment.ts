// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: any = {
  production: true,
  storckClientId: 'storck-krakow',
  clientId: 'storck-vis-local',
  realm: 'cern',
  authUrl: 'https://auth.cern.ch/auth',
  tokenExchangeUrl: 'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token',
  keyCloakUser: {
    idTokenParsed: {
      preferred_username: 'Radek Leluk',
      token: '1c437977884b572681d6f2f95e7591520e50bc47'
    }
  },
  apiUrl: 'https://localhost:8000/api'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
