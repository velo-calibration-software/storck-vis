import { Component, OnInit } from '@angular/core';
import {Store} from "../core/store.service";
import {Tab} from "../action-panel/action-panel.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  activeTab!: Tab;

  constructor(private store: Store) {
    this.activeTab = this.store.activeTab;
    this.store.changeTabEvent.subscribe(activeTab => {
      this.activeTab = activeTab;
    });
  }

}
