import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  isSignedIn: boolean = false;
  username: string = 'Test User';

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.updateUser();
  }

  onSignIn(): void {
    this.authService.login();
    this.updateUser();
  }

  onSignOut(): void {
    this.authService.logout();
    this.isSignedIn = false;
    this.username = '';
  }

  updateUser(): void {
    const loggedUser = this.authService.getLoggedUser();
    if (loggedUser) {
      this.username = loggedUser['preferred_username'];
      this.isSignedIn = true;
    }
  }

}
