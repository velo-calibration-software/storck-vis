import { NgModule, APP_INITIALIZER } from '@angular/core';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { AuthGuard } from './auth.guard';
import { AuthService } from './service/auth.service';
import { environment } from '../../environments/environment';
import { MockedKeycloakService } from '../mock/mocked-keycloak.service'
import {StorckKeycloakService} from "./service/storck-keycloak.service";

const initializeKeycloak = (keycloak: KeycloakService) => {
  return () =>
    keycloak.init({
      config: {
        url: environment.authUrl,
        realm: environment.realm,
        clientId: environment.clientId
      },
      initOptions: {
        onLoad: 'check-sso'
      },
    });
}

@NgModule({
  declarations: [],
  providers: [
    {
      provide: KeycloakService,
      useClass: environment.production ? StorckKeycloakService : MockedKeycloakService
    },
    { provide: APP_INITIALIZER, useFactory: initializeKeycloak, multi: true, deps: [KeycloakService]},
    {
      provide: AuthGuard,
      useClass: environment.production ? AuthGuard : AuthGuard
    },
    AuthService
  ],
  imports: [
    KeycloakAngularModule
  ]
})
export class AuthModule { }
