import {Component} from '@angular/core';
import {Store} from "../core/store.service";
import {Actions} from "../core/actions.service";
import {Workspace} from "../storck-defs";
import {MatDialog} from "@angular/material/dialog";
import {CredentialsDialogComponent} from "./credentials-dialog/credentials-dialog.component";
import {environment} from "../../environments/environment";

export const tabs = ['Workspaces', 'Directories', 'Filters'] as const;
export type Tab = typeof tabs[number];

@Component({
  selector: 'action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.scss']
})
export class ActionPanelComponent {
  tabs!: Tab[];
  selectedIndex: number = 0;
  worskpace: Workspace | undefined;

  constructor(public store: Store, private actions: Actions, public dialog: MatDialog) {
    this.tabs = Array.from(tabs);
    this.openTab(this.store.activeTab);

    this.store.changeWorkspaceEvent.subscribe(workspace => {
      if (this.worskpace && this.worskpace === workspace) {
        this.openTab('Directories');
      }
      this.worskpace = workspace;
    });

    this.store.openPathEvent.subscribe(() => {
      this.openTab('Directories');
    });
  }

  openTab(tab: Tab): void {
    this.selectedIndex = tabs.indexOf(tab);
  }

  changeTab(index: number) {
    this.actions.changeTab(tabs[index]);
  }

  showCredentials() {
    this.dialog.open(CredentialsDialogComponent, {
      width: '800px',
      data: {
        userToken: this.store.userToken,
        workspaceToken: this.store.workspace?.tokens[0].token,
        apiUrl: environment.apiUrl.replace('https', 'http'),
      },
    });
  }

}
