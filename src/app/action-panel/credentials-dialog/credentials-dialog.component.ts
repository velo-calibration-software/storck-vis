import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export interface CredentialsData {
  userToken: string;
  workspaceToken: string;
  apiUrl: string;
}

@Component({
  selector: 'credentials-dialog',
  templateUrl: './credentials-dialog.component.html',
  styleUrls: ['./credentials-dialog.component.scss']
})
export class CredentialsDialogComponent {
  variablesString = '';

  constructor(
    public dialogRef: MatDialogRef<CredentialsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CredentialsData,
  ) {
    this.variablesString = 'export STORCK_USER_TOKEN="' + data.userToken + '"\n'
      + 'export STORCK_WORKSPACE_TOKEN="' + data.workspaceToken + '"\n' +
      'export STORCK_API_HOST="' + data.apiUrl + '"';
  }

  copy() {
    navigator.clipboard.writeText(this.variablesString).then(() => this.dialogRef.close());
  }
}
