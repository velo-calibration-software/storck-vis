import {Injectable} from "@angular/core";
import {Store} from "./store.service";
import {Workspace} from "src/app/storck-defs";
import {File} from 'src/app/storck-defs';
import {Tab} from "../action-panel/action-panel.component";

@Injectable({providedIn: 'root'})
export class Actions {
  constructor(private store: Store) {}

  changeUserToken(newToken: string) {
    this.store.userToken = newToken;
    this.store.changeUserToken.next(newToken);
  }

  changeActivePath(newPath: string) {
    if (newPath !== this.store.activePath) {
      this.store.activePath = newPath;
      this.store.changeActivePathEvent.next(newPath);
      this.changeViewedFiles(newPath);
    }
  }

  changeViewedFiles(dirPath: string, files?: File[], expanded = true) {
    if (!files) {
      const key: number = this.store.workspace?.tokens[0] ? this.store.workspace?.tokens[0].workspace_id : -1;
      if (this.store.directoryFiles[key]) {
        files = this.store.directoryFiles[key][dirPath.normalize()] || [];
      }
      else {
        files = [];
      }
    }
    this.store.changeViewedFilesEvent.next({files, expanded});
  }

  updateDirectoriesFiles(path: string, files: File[]) {
    const key: number = this.store.workspace?.tokens[0] ? this.store.workspace?.tokens[0].workspace_id : -1;

    if (!this.store.directoryFiles[key]) {
      this.store.directoryFiles[key] = {};
    }
    this.store.directoryFiles[key][path.normalize()] = files;
  }

  changeWorkspace(workspace: Workspace) {
    this.store.workspace = workspace;
    this.store.changeWorkspaceEvent.next(workspace);
  }

  changeTab(tab: Tab) {
    this.store.activeTab = tab;
    this.store.changeTabEvent.next(tab);
  }

  openPath(path: string) {
    this.store.openPathEvent.next(path);
  }
}
