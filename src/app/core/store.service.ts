import {Injectable} from "@angular/core";
import {Subject, Subscription} from "rxjs";
import {Workspace} from "src/app/storck-defs";
import {File} from 'src/app/storck-defs';
import {Tab} from "../action-panel/action-panel.component";

@Injectable({providedIn: 'root'})
export class Store {
  changeUserToken = new Subject<string>();
  userToken: string = '';

  openPathEvent = new Subject<string>();

  changeActivePathEvent = new Subject<string>();
  activePath: string = '';

  changeViewedFilesEvent = new Subject<{ files: File[], expanded: boolean }>();
  directoryFiles: {[key: number]: {[key: string]: File[]}} = {};

  changeWorkspaceEvent = new Subject<Workspace>();
  workspace: Workspace | undefined;

  activeTab: Tab = 'Directories';
  changeTabEvent = new Subject<Tab>();

}

