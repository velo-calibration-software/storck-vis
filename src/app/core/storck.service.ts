import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, map, tap} from 'rxjs';
import {Directory, File, Schema, Workspace} from "src/app/storck-defs";
interface Response<T> {
  data: T;
}

@Injectable({providedIn: 'root'})
export class StorckService {
  constructor(private http: HttpClient) { }

  getFiles(token: string, meta_search: Object): Observable<File[]> {
    const params = new HttpParams()
      .set('token', token)
      .set('query_search', JSON.stringify(meta_search));
    return this.http.get<{ files: File[] }>('/search', { params }).pipe(map(res => res.files));
  }

  downloadFile(fileId: number, downloadName: string, token: string) {
    const params = new HttpParams()
      .set('token', token)
      .set('id', fileId);
    return this.http.get('/file', { params, responseType: 'blob' }).pipe(tap(blob => {
      const a = document.createElement('a');
      a.href = window.URL.createObjectURL(blob);
      a.download = downloadName;
      document.body.appendChild(a);
      a.click();
      a.remove();
    }));
  }

  getDirectories(path: string, token: string): Observable<Directory[]> {
    const params = new HttpParams()
      .set('token', token)
      .set('path', path);
    return this.http.get<Response<Directory[]>>('/directories', { params })
      .pipe(map(res => res.data));
  }

  getWorkspaces(): Observable<Workspace[]> {
    return this.http.get<Response<{ workspaces: Workspace[] }>>('/workspaces')
      .pipe(map(res => res.data.workspaces));
  }


  getSchema(token: string): Observable<Schema[]> {
    const params = new HttpParams().set('token', token);
    return this.http.get<{schemas: Schema[]}>('/getschema', { params })
      .pipe(map(res => res.schemas.map(schema => {
        delete schema.schema.$schema;
        return schema;
      })));
  }

  setSchema(schema: any, filetype: string, token: string): Observable<void> {
    const params = new HttpParams().set('token', token);
    const formData = new FormData();
    formData.append("metadata_schema", JSON.stringify(schema));
    formData.append("filetype", filetype);
    return this.http.post<void>('/metaschema', formData, { params });
  }

  createWorkspace(name: string): Observable<void> {
    const formData = new FormData();
    formData.append('name', name);
    return this.http.post<void>('/workspace', formData);
  }

  authenticate(): Observable<string> {
    return this.http.post<{token: string}>('/authenticate', {})
      .pipe(map(res => res.token));
  }
}
