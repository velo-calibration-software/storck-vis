import {Component, OnInit} from '@angular/core';
import {File} from 'src/app/storck-defs';
import {Store} from '../core/store.service';
import {Actions} from '../core/actions.service';

export interface FilePayload extends File {
  previousVersions: File[];
}

interface FileGroup {
  groupName: string,
  files: FilePayload[],
  expanded: boolean,
}

@Component({
  selector: 'files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.scss']
})
export class FilesListComponent implements OnInit {

  fileGroups: FileGroup[] = [];

  constructor(private store: Store, private actions: Actions) {
  }

  ngOnInit(): void {
    this.store.changeViewedFilesEvent.subscribe(({files, expanded}) => {
      this.fileGroups = [];
      files.forEach(file => {
        const dirPath = file.stored_path?.replace(/^\//, '').substring(0, file.stored_path.lastIndexOf('/') - 1) || ' ';
        const fileGroup = this.fileGroups.find(group => group.groupName === dirPath);
        if (!file.hide) {
          const filePayload: FilePayload = {...file, previousVersions: []};
          let previousVersionId = filePayload.previous_version;
          while (previousVersionId) {
            const previousVersionFile = files.find(f => f.id === previousVersionId);
            if (previousVersionFile) {
              filePayload.previousVersions.push(previousVersionFile);
              previousVersionId = previousVersionFile.previous_version;
            }
            else {
              break;
            }
          }
          if (fileGroup) {
            fileGroup.files.push(filePayload);
          } else {
            this.fileGroups.push({groupName: dirPath, files: [filePayload], expanded});
          }
        }
      });
    });
  }

  openPath(event: Event, fileGroup: FileGroup, index: number) {
    event.stopPropagation();
    const path = '/' + fileGroup.groupName.split('/').slice(0, index + 1).join('/');
    this.actions.openPath(path);
  }
}
